export class CreatePlanogramModel
{
    constructor(public store_id: number = null,
                public product_category_id: number = null,
                public accurate: string = null,
                public no_reason: string = null,
                public images: { fileName: string, file: Blob }[] = [])
    {
    }
}
