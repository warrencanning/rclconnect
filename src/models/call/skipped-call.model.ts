export class SkippedCallModel
{
    constructor(public store_call_id: number = null,
                public reason: string = null)
    {
    }
}
