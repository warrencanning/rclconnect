export class CreateDoneCallModel
{
    constructor(public store_call_id: number = null,
                public latitude: number = null,
                public longitude: number = null,
                public check_in: string = null,
                public check_out: string = null)
    {
    }
}
