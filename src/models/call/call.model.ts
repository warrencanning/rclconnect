export interface CallModel
{
    id: number;
    day: string;
    frequency: number;
}
