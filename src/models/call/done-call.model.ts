export interface DoneCallModel
{
    id: number;
    latitude: number | null;
    longitude: number | null;
    check_in: string;
    check_out: string;
}
