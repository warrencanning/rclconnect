import { CallModel, DoneCallModel, StoreContactModel, StoreTypeModel } from '..';

export interface StoreModel
{
    id: number;
    name: string;
    call: CallModel;
    type: StoreTypeModel;
    latest_done_call: DoneCallModel;
    contact: StoreContactModel;
}
