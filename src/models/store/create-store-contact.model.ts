export class CreateStoreContactModel
{
    constructor(public store_id: number = null,
                public manager: string = null,
                public phone: string = null,
                public address: string = null,
                public out_of_stocks: string = null,
                public last_order: string = null,
                public order_day: string = null,
                public delivery_day: string = null)
    {
    }
}
