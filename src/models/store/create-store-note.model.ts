export class CreateStoreNoteModel
{
    store_id: number;
    note: string;
}
