export interface StoreNoteModel
{
    id: number;
    note: string;
}
