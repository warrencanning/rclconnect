export interface StoreContactModel
{
    id: number;
    manager: string;
    phone: string;
    address: string;
    out_of_stocks: string;
    last_order: string;
    order_day: string;
    delivery_day: string;
}
