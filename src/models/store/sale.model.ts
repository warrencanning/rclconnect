export interface SaleModel
{
    id: number;
    month: number;
    percentage_achieved_month: string;
    remaining_cases_for_month: string;
    year: number;
    percentage_achieved_year: string;
    remaining_cases_for_year: string;
}
