export class CreateForwardShareModel
{
    constructor(public store_id: number = null,
                public product_sub_category_id: number = null,
                public category_facings: number = null,
                public category_facings_percent: number = null,
                public competitor_facings: number = null,
                public competitor_facings_percent: number = null,
                public target: number = null,
                public signature: Blob = null)
    {
    }
}
