import { ProductSubCategoryModel } from './product-sub-category.model';

export interface ProductCategoryModel
{
    id: number;
    category: string;
    sub_categories: ProductSubCategoryModel[];
}
