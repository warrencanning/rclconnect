export interface ProductSubCategoryModel
{
    id: number;
    sub_category: string;
}
