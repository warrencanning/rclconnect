export interface UserModel
{
    email: string;
    id: number;
    name: string;
    phone: string;
    user_role_id: number;
}
