export interface DashboardModel
{
    target_calls: number;
    done_calls: number;
    done_calls_percent: number;
    total_sales: number;
}
