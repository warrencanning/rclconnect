export class CreatePromotionExecutionModel
{
    constructor(public store_id: number = null,
                public promotion_id: number = null,
                public product_category_id: number = null,
                public execution: string = null,
                public no_reason: string = null,
                public images: { fileName: string, file: Blob }[] = [])
    {
    }
}
