import { QuestionTypeModel } from './question-type.model';
import { QuestionAnswerModel } from './question-answer.model';

export interface QuestionnaireModel
{
    id: number;
    question: string;
    show_when_answer_id: number | null;
    type: QuestionTypeModel;
    answers: QuestionAnswerModel[];
    user_answer: QuestionAnswerModel;
    score: number;
}
