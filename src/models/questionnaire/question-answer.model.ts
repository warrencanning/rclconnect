export interface QuestionAnswerModel
{
    id: number;
    answer: string;
    score: number;
}
