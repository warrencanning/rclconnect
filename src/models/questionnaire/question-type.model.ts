export interface QuestionTypeModel
{
    id: number;
    type: string;
}
