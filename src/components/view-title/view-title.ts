import { Component, Input } from '@angular/core';

@Component({
    selector: 'view-title',
    templateUrl: 'view-title.html'
})
export class ViewTitleComponent
{
    @Input('title') title: string;
}
