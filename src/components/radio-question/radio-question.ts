import { Component, EventEmitter, Input, Output } from '@angular/core';

import { QuestionnaireModel } from '../../models';

@Component({
    selector: 'radio-question',
    templateUrl: 'radio-question.html'
})
export class RadioQuestionComponent
{
    @Input('question') question: QuestionnaireModel;
    @Output() scoreAmountChanged = new EventEmitter<boolean>();

    calculateScore()
    {
        this.scoreAmountChanged.emit(true);
    }
}
