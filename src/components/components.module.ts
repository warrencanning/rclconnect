import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from 'ionic-angular';

import { ViewTitleComponent } from './view-title/view-title';
import { RadioQuestionComponent } from './radio-question/radio-question';

@NgModule({
    declarations: [
        ViewTitleComponent,
        RadioQuestionComponent
    ],
    imports: [CommonModule, IonicModule],
    exports: [
        ViewTitleComponent,
        RadioQuestionComponent
    ]
})
export class ComponentsModule
{
}
