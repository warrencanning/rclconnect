import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import {
    CallModel,
    CreateDoneCallModel, CreateForwardShareModel, CreateStoreContactModel,
    CreateStoreNoteModel, SaleModel,
    SkippedCallModel, StoreContactModel,
    StoreModel,
    StoreNoteModel,
    StoreTypeModel
} from '../../models';

@Injectable()
export class StoreProvider
{
    private url = '/v1/stores';

    constructor(private http: HttpClient)
    {
    }

    getStoreTypes(): Observable<StoreTypeModel[]>
    {
        const url = `${this.url}/types`;

        return this.http
            .get<StoreTypeModel[]>(url);
    }

    getStores(day: string = null): Observable<StoreModel[]>
    {
        let url = this.url;

        if (day)
        {
            url += `/${day}`;
        }

        return this.http
            .get<StoreModel[]>(url);
    }

    skipCall(skippedCall: SkippedCallModel): Observable<null>
    {
        let url = `${this.url}/calls/skip`;

        return this.http
            .post<null>(url, skippedCall);
    }

    createCall(createDoneCallModel: CreateDoneCallModel): Observable<CallModel>
    {
        let url = `${this.url}/calls`;

        return this.http
            .post<CallModel>(url, createDoneCallModel);
    }

    createForwardShare(createForwardShareModel: CreateForwardShareModel): Observable<null>
    {
        let url = `${this.url}/forwardShare`;
        const formData = new FormData();
        formData.append('store_id', createForwardShareModel.store_id.toString());
        formData.append('product_sub_category_id', createForwardShareModel.product_sub_category_id.toString());
        formData.append('category_facings', createForwardShareModel.category_facings.toString());

        if (createForwardShareModel.competitor_facings)
        {
            formData.append('competitor_facings', createForwardShareModel.competitor_facings.toString());
        }
        formData.append('signature', createForwardShareModel.signature);

        return this.http
            .post<null>(url, formData);
    }

    update(store: StoreModel): Observable<StoreModel>
    {
        return this.http
            .put<StoreModel>(this.url, store);
    }

    createStoreContact(createStoreContactModel: CreateStoreContactModel): Observable<StoreContactModel>
    {
        const url = `${this.url}/contacts`;

        return this.http
            .post<StoreContactModel>(url, createStoreContactModel);
    }

    getLastNote(): Observable<StoreNoteModel>
    {
        const url = `${this.url}/notes/last`;

        return this.http
            .get<StoreNoteModel>(url);
    }

    createNote(createNoteModel: CreateStoreNoteModel): Observable<StoreNoteModel>
    {
        const url = `${this.url}/notes`;

        return this.http
            .post<StoreNoteModel>(url, createNoteModel);
    }

    getSalesData(storeId: number): Observable<SaleModel>
    {
        const url = `${this.url}/sales/${storeId}`;

        return this.http
            .get<SaleModel>(url);
    }
}
