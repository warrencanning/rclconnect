import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ProductCategoryModel, ProductModel } from '../../models';

@Injectable()
export class ProductProvider
{
    private url = '/v1/products';

    constructor(private http: HttpClient)
    {
    }

    getProducts(): Observable<ProductModel[]>
    {
        const url = `${this.url}`;

        return this.http
            .get<ProductModel[]>(url);
    }

    getProductCategories(): Observable<ProductCategoryModel[]>
    {
        const url = `${this.url}/categories`;

        return this.http
            .get<ProductCategoryModel[]>(url);
    }
}
