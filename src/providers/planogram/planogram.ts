import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import {
    CreatePlanogramModel,
    PlanogramModel
} from '../../models';

@Injectable()
export class PlanogramProvider
{
    private url = '/v1/planograms';

    constructor(private http: HttpClient)
    {
    }

    createPlanogram(createPlanogramModel: CreatePlanogramModel): Observable<PlanogramModel>
    {
        const url = `${this.url}`;

        return this.http
            .post<PlanogramModel>(url, createPlanogramModel)
    }
}
