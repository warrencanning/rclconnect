import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class AlertProvider
{
    constructor(private toastCtrl: ToastController)
    {
    }

    showMessage(message = 'Some error, please try again')
    {
        this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'bottom',
            showCloseButton: true,
            closeButtonText: 'OK'
        }).present();
    }

    showErrorFromResponse(response: HttpErrorResponse)
    {
        const message = this.getErrorMessageFromResponse(response);
        this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'bottom',
            showCloseButton: true,
            closeButtonText: 'OK'
        }).present();
    }

    putErrorFromResponseToConsole(methodName: string, pageName: string, response: HttpErrorResponse)
    {
        pageName = pageName.charAt(0).toUpperCase() + pageName.slice(1);

        if (pageName.toLowerCase().indexOf('page') < 0)
        {
            pageName += 'Page';
        }

        const errorDescription = `Error ${methodName} on ${pageName}`;
        console.error(errorDescription, response);
    }

    private getErrorMessageFromResponse(response: HttpErrorResponse): string
    {
        let message = '';

        if (response.error.message)
        {
            message = response.error.message;
        }

        if (response.error.errors)
        {
            response.error.errors.forEach((error: string) =>
            {
                message = message.concat(error);
                message += ' ';
            });
        }

        if (!message)
        {
            message = 'Some error, please try again';
        }

        return message;
    }
}
