import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { LocalStorageProvider } from './local-storage/local-storage';

@Injectable()
export class HttpsRequestInterceptor implements HttpInterceptor
{
    /**
     * @param {HttpRequest<any>} req
     * @param {HttpHandler} next
     *
     * @returns {Observable<HttpEvent<any>>}
     */
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>
    {
        // Get the auth header from the service.
        const token = LocalStorageProvider.getToken();
        const url = req.url.indexOf(LocalStorageProvider.getDomain()) > -1 ?
            req.url :
            LocalStorageProvider.getDomain() + req.url + '';

        if (!token)
        {
            const apiReq = req.clone({
                setHeaders: { 'Accept': 'application/json' },
                url: url
            });

            return next.handle(apiReq);
        }
        else
        {
            // Get the auth header from the service.
            const authHeader = 'Bearer ' + token;
            // Clone the request to add the new header.
            const authReq = req.clone({
                setHeaders: { 'Authorization': authHeader, 'Accept': 'application/json' },
                url: url
            });
            // Pass on the cloned request instead of the original request.
            return next.handle(authReq);
        }
    }
}
