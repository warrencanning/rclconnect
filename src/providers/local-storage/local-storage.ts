import { Injectable } from '@angular/core';

import {
    ProductCategoryModel,
    ProductModel,
    PromotionModel,
    QuestionnaireModel, StoreNoteModel,
    StoreTypeModel,
    UserModel
} from '../../models';

const domains = {
    production: 'https://rcl.connectfmcg.co.za/public/api', // without slash at the end
    dev: 'http://rcl-backend/api', // without slash at the end
};

@Injectable()
export class LocalStorageProvider
{
    static set(key: string, data: any)
    {
        if (data != null && typeof data != 'undefined')
        {
            localStorage.setItem(key, JSON.stringify(data));
        }
    }

    static get(key: string): any
    {
        const data = localStorage.getItem(key);

        if (!data || data === 'undefined' || data === '{}')
        {
            return null;
        }

        return JSON.parse(data);
    }

    static remove(key: string)
    {
        localStorage.removeItem(key);
    }

    static getDomain(): string
    {
        return domains.production;
    }

    static getToken(): string
    {
        return this.get('token');
    }

    static login(data: { token: string, user: UserModel })
    {
        this.set('token', data.token);
        this.setUser(data.user);
    }

    static getUser(): UserModel
    {
        return this.get('user');
    }

    static setUser(user: UserModel)
    {
        this.set('user', user);
    }

    static setProductCategories(categories: ProductCategoryModel[])
    {
        this.set('productCategories', categories);
    }

    static getProductCategories(): ProductCategoryModel[]
    {
        return this.get('productCategories');
    }

    static setProducts(products: ProductModel[])
    {
        this.set('products', products);
    }

    static getProducts(): ProductModel[]
    {
        return this.get('products');
    }

    static setQuestionnaire(questionnaire: QuestionnaireModel[])
    {
        this.set('questionnaire', questionnaire);
    }

    static getQuestionnaire(): QuestionnaireModel[]
    {
        return this.get('questionnaire');
    }

    static setLastStoreNote(note: StoreNoteModel)
    {
        this.set('note', note);
    }

    static getLastStoreNote(): StoreNoteModel
    {
        return this.get('note');
    }

    static setPromotions(promotions: PromotionModel[])
    {
        this.set('promotions', promotions);
    }

    static getPromotions(): PromotionModel[]
    {
        return this.get('promotions');
    }

    static setStoreTypes(storeTypes: StoreTypeModel[])
    {
        this.set('storeTypes', storeTypes);
    }

    static getStoreTypes(): StoreTypeModel[]
    {
        return this.get('storeTypes');
    }

    static logout()
    {
        localStorage.clear();
    }
}
