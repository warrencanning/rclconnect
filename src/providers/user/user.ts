import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { DashboardModel } from '../../models';

@Injectable()
export class UserProvider
{
    private url = '/v1/users';

    constructor(private http: HttpClient)
    {
    }

    getDashboardData(): Observable<DashboardModel>
    {
        const url = `${this.url}/getDashboardData`;

        return this.http
            .get<DashboardModel>(url);
    }
}
