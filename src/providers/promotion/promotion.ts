import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { CreatePromotionExecutionModel, PromotionExecutionModel, PromotionModel } from '../../models';

@Injectable()
export class PromotionProvider
{
    private url = '/v1/promotions';

    constructor(private http: HttpClient)
    {
    }

    getPromotions(): Observable<PromotionModel[]>
    {
        return this.http
            .get<PromotionModel[]>(this.url);
    }

    createPromotionExecution(createPromotionExecutionModel: CreatePromotionExecutionModel): Observable<PromotionExecutionModel>
    {
        const url = `${this.url}/executions`;

        return this.http
            .post<PromotionExecutionModel>(url, createPromotionExecutionModel)
    }
}
