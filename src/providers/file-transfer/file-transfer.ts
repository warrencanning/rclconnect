import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Entry, File } from '@ionic-native/file';

@Injectable()
export class FileTransferProvider
{
    constructor(private http: HttpClient,
                private file: File)
    {
    }

    makeFileIntoBlob(imgUri: string): Promise<{ fileName: string, file: Blob }>
    {
        return new Promise((resolve, reject) =>
        {
            let fileName = '';

            this.file.resolveLocalFilesystemUrl(imgUri)
                .then((fileEntry: Entry) =>
                {
                    const nativeURL = fileEntry.nativeURL;
                    fileName = fileEntry.name;

                    // get the path..
                    const path = nativeURL.substring(0, nativeURL.lastIndexOf('/'));

                    // we are provided the name, so now read the file into
                    // a buffer
                    return this.file.readAsArrayBuffer(path, fileName);
                })
                .then((buffer: ArrayBuffer) =>
                {
                    // get the buffer and make a blob to be saved
                    const file = new Blob([buffer], { type: 'image/jpeg' });

                    resolve({ fileName, file });
                })
                .catch((error) => reject(error));
        });
    }

    sendFiles(files: { fileName: string, file: Blob }[], url: string): Observable<null>
    {
        const formData = new FormData();

        for (let i = 0; i < files.length; i++)
        {
            formData.append('files[]', files[i].file);
        }

        return this.http
            .post<null>(url, formData);
    }
}
