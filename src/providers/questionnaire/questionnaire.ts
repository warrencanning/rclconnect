import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { QuestionnaireModel } from '../../models';

@Injectable()
export class QuestionnaireProvider
{
    private url = '/v1/questionnaire';

    constructor(private http: HttpClient)
    {
    }

    getQuestionnaire(): Observable<QuestionnaireModel[]>
    {
        return this.http
            .get<QuestionnaireModel[]>(this.url);
    }

    createScoreCard(createScoreCardModel): Observable<null>
    {
        let url = `${this.url}`;

        return this.http
            .post<null>(url, createScoreCardModel);
    }
}
