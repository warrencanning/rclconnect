import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { LoginModel, UserModel } from '../../models';

@Injectable()
export class AuthProvider
{
    isUserLoggedIn = new EventEmitter<boolean>();

    private url = '/v1/auth';

    constructor(private http: HttpClient)
    {
    }

    userWasLoggedIn()
    {
        this.isUserLoggedIn.emit(true);
    }

    userWasLoggedOut()
    {
        this.isUserLoggedIn.emit(false);
    }

    login(credentials: LoginModel): Observable<{ user: UserModel, token: string }>
    {
        const url = `${this.url}/login`;

        return this.http
            .post<{ user: UserModel, token: string }>(url, credentials);
    }

    logout(): Observable<null>
    {
        const url = `${this.url}/logout`;

        return this.http
            .get<null>(url);
    }
}
