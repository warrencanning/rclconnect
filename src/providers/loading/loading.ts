import { Injectable } from '@angular/core';
import { Loading, LoadingController, LoadingOptions } from 'ionic-angular';

/**
 * This is the provider class for most of the loading spinners screens on the app.
 */
@Injectable()
export class LoadingProvider
{
    /**
     * Set your spinner/loading indicator type here
     * List of Spinners: https://ionicframework.com/docs/api/components/spinner/Spinner/
     *
     * @type {{spinner: string}}
     */
    private spinner: LoadingOptions = {
        spinner: 'circles'
    };
    private loading: Loading;

    constructor(private loadingController: LoadingController)
    {
    }

    /**
     * Show loading
     */
    show(content?: string)
    {
        if (!this.loading)
        {
            if (content)
            {
                this.spinner.content = content;
            }
            else
            {
                this.spinner.content = '';
            }

            this.loading = this.loadingController.create(this.spinner);
            this.loading.present();
        }
    }

    /**
     * Hide loading
     */
    hide()
    {
        if (this.loading)
        {
            this.loading.dismiss();
            this.loading = null;
        }
    }
}
