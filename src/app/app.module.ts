import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, Injectable, Injector, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { Pro } from '@ionic/pro';

import { MyApp } from './app.component';

import {
    AuthProvider,
    AlertProvider,
    LoadingProvider,
    UserProvider,
    HttpsRequestInterceptor,
    StoreProvider,
    QuestionnaireProvider,
    ProductProvider,
    PromotionProvider,
    PlanogramProvider,
    FileTransferProvider
} from '../providers';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Keyboard } from '@ionic-native/keyboard';
import { BackgroundMode } from '@ionic-native/background-mode';
import { Geolocation } from '@ionic-native/geolocation';
import { OneSignal } from '@ionic-native/onesignal';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';

Pro.init('44258c50', {
    appVersion: '0.5.5'
});

@Injectable()
export class MyErrorHandler implements ErrorHandler
{
    ionicErrorHandler: IonicErrorHandler;

    constructor(injector: Injector)
    {
        try
        {
            this.ionicErrorHandler = injector.get(IonicErrorHandler);
        }
        catch (e)
        {
            // Unable to get the IonicErrorHandler provider, ensure
            // IonicErrorHandler has been added to the providers list below
        }
    }

    handleError(err: any): void
    {
        Pro.monitoring.handleNewError(err);
        // Remove this if you want to disable Ionic's auto exception handling
        // in development mode.
        this.ionicErrorHandler && this.ionicErrorHandler.handleError(err);
    }
}

@NgModule({
    declarations: [
        MyApp
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        IonicModule.forRoot(MyApp),
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp
    ],
    providers: [
        StatusBar,
        SplashScreen,
        OneSignal,
        BackgroundMode,
        Keyboard,
        Geolocation,
        Camera,
        File,
        IonicErrorHandler,
        [{ provide: ErrorHandler, useClass: MyErrorHandler }],
        { provide: HTTP_INTERCEPTORS, useClass: HttpsRequestInterceptor, multi: true },
        UserProvider,
        AlertProvider,
        LoadingProvider,
        AuthProvider,
        StoreProvider,
        QuestionnaireProvider,
        ProductProvider,
        PromotionProvider,
        FileTransferProvider,
        PlanogramProvider
    ]
})
export class AppModule
{
}
