import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { finalize } from 'rxjs/operators';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { OneSignal, OSNotification, OSNotificationOpenedResult } from '@ionic-native/onesignal';
import { Keyboard } from '@ionic-native/keyboard';
import { BackgroundMode } from '@ionic-native/background-mode';

import {
    AlertProvider,
    AuthProvider,
    LoadingProvider,
    LocalStorageProvider
} from '../providers';

@Component({
    templateUrl: 'app.html'
})
export class MyApp
{
    @ViewChild(Nav) nav: Nav;

    rootPage: any;
    isUserLoggedIn: boolean;
    pages: { title: string, component: any }[];

    private oneSignalCreds = {
        appId: 'df9b9088-340b-4da7-8e61-11bc5678861d',
        googleProjectNumber: '859408745101'
    };

    constructor(private platform: Platform,
                private statusBar: StatusBar,
                private authProvider: AuthProvider,
                private splashScreen: SplashScreen,
                private oneSignal: OneSignal,
                private alertProvider: AlertProvider,
                private loadingProvider: LoadingProvider,
                private keyboard: Keyboard,
                private backgroundMode: BackgroundMode)
    {
        this.initializeApp();

        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Home', component: 'HomePage' }
        ];
    }

    private initializeApp()
    {
        this.platform.ready()
            .then(() =>
            {
                if (this.platform.is('cordova'))
                {
                    this.initNative();
                }

                this.setStartPage();
            });
    }

    private initNative()
    {
        // Okay, so the platform is ready and our plugins are available.
        // Here you can do any higher level native things you might need.
        this.initOneSignal();
        this.statusBar.overlaysWebView(false);
        this.statusBar.styleLightContent();
        this.statusBar.backgroundColorByHexString('#000000');
        this.splashScreen.hide();
        this.keyboard.hideFormAccessoryBar(false);
        this.backgroundMode.enable();
    }

    private initOneSignal()
    {
        this.oneSignal.startInit(this.oneSignalCreds.appId, this.oneSignalCreds.googleProjectNumber);

        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.None);

        this.oneSignal.handleNotificationReceived().subscribe((notification: OSNotification) =>
        {
            console.log('do something when notification is received', notification);
        });

        this.oneSignal.handleNotificationOpened().subscribe((result: OSNotificationOpenedResult) =>
        {
            console.log('do something when a notification is opened', result);
        });

        this.oneSignal.endInit();
    }

    private checkIsUserLoggedIn()
    {
        this.authProvider.isUserLoggedIn
            .subscribe((status: boolean) =>
            {
                this.isUserLoggedIn = status;
            });
    }

    private setStartPage()
    {
        const user = LocalStorageProvider.getUser();

        if (user)
        {
            this.rootPage = 'HomePage';
        }
        else
        {
            this.rootPage = 'LoginPage';
        }

        this.checkIsUserLoggedIn();
    }

    openPage(page: { title: string, component: any })
    {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    }

    logout()
    {
        this.loadingProvider.show();
        this.authProvider.logout()
            .pipe(finalize(() => this.loadingProvider.hide()))
            .subscribe(() =>
            {
                LocalStorageProvider.logout();
                this.nav.setRoot('LoginPage');
            }, (error) =>
            {
                this.alertProvider.putErrorFromResponseToConsole('logout', 'MyApp', error);
                this.alertProvider.showErrorFromResponse(error);
            });
    }
}

