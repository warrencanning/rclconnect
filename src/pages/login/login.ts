import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { finalize } from 'rxjs/operators';

import {
    LoadingProvider,
    LocalStorageProvider,
    AlertProvider,
    AuthProvider
} from '../../providers';

import {
    LoginModel,
    UserModel
} from '../../models';

@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html'
})
export class LoginPage
{
    credentials = new LoginModel();

    constructor(private navCtrl: NavController,
                private authProvider: AuthProvider,
                private loadingProvider: LoadingProvider,
                private alertProvider: AlertProvider)
    {
    }

    ionViewDidEnter()
    {
        this.authProvider.userWasLoggedOut();
    }

    login()
    {
        this.loadingProvider.show();
        this.authProvider.login(this.credentials)
            .pipe(finalize(() => this.loadingProvider.hide()))
            .subscribe((data: { user: UserModel, token: string }) =>
            {
                LocalStorageProvider.login(data);
                this.authProvider.userWasLoggedIn();
                this.navCtrl.setRoot('HomePage');
            }, (error) =>
            {
                this.alertProvider.putErrorFromResponseToConsole('login', 'LoginPage', error);
                this.alertProvider.showErrorFromResponse(error);
            });
    }

    forgotPassword()
    {
//         this.navCtrl.push('ForgotPasswordPage');
    }
}
