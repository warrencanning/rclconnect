import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

@IonicPage()
@Component({
    selector: 'page-image-viewer',
    templateUrl: 'image-viewer.html',
})
export class ImageViewerPage
{
    imageUrls = [
        {url: './assets/imgs/frozenfish1.jpg', subtitle: 'first'},
        {url: './assets/imgs/frozenfish2.jpg', subtitle: 'second'}
    ];

    constructor(private navCtrl: NavController)
    {
    }

    ionViewDidLoad()
    {
    }

    close()
    {
        this.navCtrl.pop();
    }
}
