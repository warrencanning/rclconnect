import { Component } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { finalize } from 'rxjs/operators';

import { SkippedCallModel, StoreModel } from '../../models';

import { AlertProvider, LoadingProvider, StoreProvider } from '../../providers';

@IonicPage()
@Component({
    selector: 'page-stores',
    templateUrl: 'stores.html',
})
export class StoresPage
{
    filteredStores: StoreModel[] = [];
    allStores: StoreModel[] = [];

    constructor(private navCtrl: NavController,
                private navParams: NavParams,
                private storeProvider: StoreProvider,
                private alertProvider: AlertProvider,
                private loadingProvider: LoadingProvider,
                private alertCtrl: AlertController)
    {
    }

    ionViewDidLoad()
    {
        this.allStores = this.navParams.get('stores');
        this.filteredStores = this.allStores;
    }

    filterStores(event)
    {
        if (this.allStores && this.allStores.length < 1)
        {
            return;
        }

        const value = event.target.value.toLowerCase();

        if (value === '' || !value)
        {
            this.filteredStores = this.allStores;
        }

        this.filteredStores = this.allStores.filter((store: StoreModel) =>
        {
            return store.name.toLowerCase().indexOf(value) > -1;
        });
    }

    skipCall(callId: number)
    {
        this.alertCtrl.create({
            title: 'Reason',
            inputs: [
                { type: 'radio', label: 'Office Meetings', value: 'Office Meetings', checked: true },
                { type: 'radio', label: 'Training', value: 'Training' },
                { type: 'radio', label: 'Car Service', value: 'Car Service' },
                { type: 'radio', label: 'New Store Opening', value: 'New Store Opening' },
                { type: 'radio', label: 'Calling on Another Store', value: 'Calling on Another Store' },
                { type: 'radio', label: 'Out of Time', value: 'Out of Time' },
                { type: 'radio', label: 'Admin', value: 'Admin' },
                { type: 'radio', label: 'Delivering POS', value: 'Delivering POS' },
                { type: 'radio', label: 'Free Stock Delivery', value: 'Free Stock Delivery' },
                { type: 'radio', label: 'Principal Request', value: 'Principal Request' }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel'
                },
                {
                    text: 'Ok',
                    handler: (reason: string) =>
                    {
                        const skippedCall = new SkippedCallModel(callId, reason);
                        this.sendSkipCall(skippedCall);
                    }
                }
            ]
        }).present();
    }

    private sendSkipCall(skippedCall: SkippedCallModel)
    {
        this.loadingProvider.show();
        this.storeProvider.skipCall(skippedCall)
            .pipe(finalize(() => this.loadingProvider.hide()))
            .subscribe(() =>
            {
                this.alertProvider.showMessage('Skipped');
            }, (error) =>
            {
                this.alertProvider.putErrorFromResponseToConsole('sendSkipCall', 'StoresPage', error);
                this.alertProvider.showErrorFromResponse(error);
            });
    }

    openStorePage(store: StoreModel)
    {
        this.navCtrl.push('StorePage', { store: store });
    }
}
