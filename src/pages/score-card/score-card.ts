import { Component, ViewChild } from '@angular/core';
import { AlertController, Content, IonicPage, NavController, NavParams } from 'ionic-angular';

import { AlertProvider, LocalStorageProvider } from '../../providers';

import { ProductModel, QuestionnaireModel, StoreModel } from '../../models';

@IonicPage()
@Component({
    selector: 'page-score-card',
    templateUrl: 'score-card.html',
})
export class ScoreCardPage
{
    @ViewChild(Content) content: Content;

    questionnaire: QuestionnaireModel[];
    selectedProduct: ProductModel;
    scoreAmount = 0;

    scoreCard: {
        product: ProductModel,
        store: StoreModel,
        questions: {
            title: string,
            type: string,
            answers: { value: string, score: number, additional: boolean }[],
            userAnswer: { value: string, score: number, additional: boolean },
            additionalQuestion: {
                title: string,
                answers: { value: string, score: number }[],
                userAnswer: { value: string, score: number }
            }
        }[]
    }[];

    questions: {
        title: string,
        type: string,
        answers: { value: string, score: number, additional: boolean }[],
        userAnswer: { value: string, score: number, additional: boolean },
        additionalQuestion: {
            title: string,
            answers: { value: string, score: number }[],
            userAnswer: { value: string, score: number }
        }
    }[];

    additionalQuestions: {
        title: string,
        type: string,
        answers: { value: string, score: number, additional: boolean }[],
        userAnswer: { value: string, score: number, additional: boolean },
        additionalQuestion: {
            title: string,
            answers: { value: string, score: number }[],
            userAnswer: { value: string, score: number }
        }
    }[];

    private filteredProducts: ProductModel[];
    private allProducts: ProductModel[];
    private store: StoreModel;

    constructor(private navCtrl: NavController,
                private navParams: NavParams,
                private alertProvider: AlertProvider,
                private alertCtrl: AlertController)
    {
    }

    ionViewDidLoad()
    {
        this.questionnaire = LocalStorageProvider.getQuestionnaire();
        this.allProducts = LocalStorageProvider.getProducts();
        this.store = this.navParams.get('store');
        this.filteredProducts = this.allProducts;
        this.selectedProduct = this.allProducts[0];
        this.setupQuestions();
        this.filterProducts();
        this.setupAdditionalQuestions();
    }

    private setupQuestions()
    {
        this.questions =
            [
                {
                    title: 'Is stock on shelf?',
                    type: 'select',
                    answers: [
                        { value: 'Yes', score: 1, additional: false },
                        { value: 'No', score: 0, additional: true }
                    ],
                    userAnswer: { value: null, score: null, additional: false },
                    additionalQuestion: {
                        title: 'If No, Select Reason',
                        answers: [
                            { value: 'Store', score: 1 },
                            { value: 'Delivery', score: 1 },
                            { value: 'Supplier issue', score: 1 },
                            { value: 'Field staff issue', score: 0 },
                            { value: 'Not Ranged', score: 1 }
                        ],
                        userAnswer: { value: null, score: null }
                    }
                },
                {
                    title: 'Low Pressure stock',
                    type: 'select',
                    answers: [
                        { value: 'Yes', score: 1, additional: false },
                        { value: 'Not Ranged', score: 1, additional: false },
                        { value: 'No', score: 0, additional: true }
                    ],
                    userAnswer: { value: null, score: null, additional: false },
                    additionalQuestion: {
                        title: 'If No, Select Reason',
                        answers: [
                            { value: 'ROS/MSS', score: 1 },
                            { value: 'Store', score: 1 },
                            { value: 'Delivery', score: 1 },
                            { value: 'Supplier issue', score: 1 },
                            { value: 'Field staff issue', score: 0 }
                        ],
                        userAnswer: { value: null, score: null }
                    }
                },
                {
                    title: 'Are all PI labels on shelf, updated for current month and placed correctly?',
                    type: null,
                    answers: [
                        { value: 'Yes', score: 1, additional: false },
                        { value: 'Not Ranged', score: 1, additional: false },
                        { value: 'No', score: 0, additional: false }
                    ],
                    userAnswer: { value: null, score: null, additional: false },
                    additionalQuestion: null
                },
                {
                    title: 'Any damaged product on shelf?',
                    type: 'photo',
                    answers: [
                        { value: 'Yes', score: 0, additional: true },
                        { value: 'Not Ranged', score: 1, additional: false },
                        { value: 'No', score: 1, additional: false }
                    ],
                    userAnswer: { value: null, score: null, additional: false },
                    additionalQuestion: {
                        title: 'If Yes, Take a photo',
                        answers: null,
                        userAnswer: { value: null, score: null }
                    }
                }
            ];

        if (!this.scoreCard)
        {
            this.scoreCard = [{ store: this.store, product: this.selectedProduct, questions: this.questions }];
        }
        else
        {
            this.scoreCard.push({ store: this.store, product: this.selectedProduct, questions: this.questions });
        }
    }

    private setupAdditionalQuestions()
    {
        this.additionalQuestions = [
            {
                title: 'Is stock rotation correct?',
                type: null,
                answers: [
                    { value: 'Yes', score: 1, additional: false },
                    { value: 'No', score: 0, additional: false }
                ],
                userAnswer: { value: null, score: null, additional: false },
                additionalQuestion: null
            },
            {
                title: 'Is brand flow correct?',
                type: 'select',
                answers: [
                    { value: 'Yes', score: 1, additional: false },
                    { value: 'No', score: 0, additional: true }
                ],
                userAnswer: { value: null, score: null, additional: false },
                additionalQuestion: {
                    title: 'If No, Select Reason',
                    answers: [
                        { value: 'PNP PLANO', score: 1 },
                        { value: 'SPAR PLANO', score: 1 },
                        { value: 'Field staff error', score: 0 }
                    ],
                    userAnswer: { value: null, score: null }
                }
            },
            {
                title: 'Are shelves neat & clean?',
                type: null,
                answers: [
                    { value: 'Yes', score: 1, additional: false },
                    { value: 'No', score: 0, additional: false }
                ],
                userAnswer: { value: null, score: null, additional: false },
                additionalQuestion: null
            },
            {
                title: 'Are price flashes on all promo lines?',
                type: 'select',
                answers: [
                    { value: 'Yes', score: 1, additional: false },
                    { value: 'N/A - No Promo lines', score: 1, additional: false },
                    { value: 'No', score: 0, additional: true }
                ],
                userAnswer: { value: null, score: null, additional: false },
                additionalQuestion: {
                    title: 'If No, Select Reason',
                    answers: [
                        { value: 'Store Supplier issue', score: 1 },
                        { value: 'Field staff issue', score: 0 }
                    ],
                    userAnswer: { value: null, score: null }
                }
            },
            {
                title: 'Is POS up on promo lines?',
                type: 'select',
                answers: [
                    { value: 'Yes', score: 1, additional: false },
                    { value: 'N/A - No Promo lines', score: 1, additional: false },
                    { value: 'No', score: 0, additional: true }
                ],
                userAnswer: { value: null, score: null, additional: false },
                additionalQuestion: {
                    title: 'If No, Select Reason',
                    answers: [
                        { value: 'Store Supplier issue', score: 1 },
                        { value: 'Field staff issue', score: 0 }
                    ],
                    userAnswer: { value: null, score: null }
                }
            },
            {
                title: 'POS Smart shopper (PNP only)',
                type: 'select',
                answers: [
                    { value: 'Yes', score: 1, additional: false },
                    { value: 'N/A - not a PNP store', score: 1, additional: false },
                    { value: 'No', score: 0, additional: true }
                ],
                userAnswer: { value: null, score: null, additional: false },
                additionalQuestion: {
                    title: 'If No, Select Reason',
                    answers: [
                        { value: 'Store Claims did not get POS', score: 1 },
                        { value: 'Store Refuses', score: 1 },
                        { value: 'VTM has not installed POS', score: 0 },
                    ],
                    userAnswer: { value: null, score: null }
                }
            },
            {
                title: 'Is paid Blitz end up?',
                type: 'select',
                answers: [
                    { value: 'Yes', score: 1, additional: false },
                    { value: 'N/A - no paid Blitz end', score: 1, additional: false },
                    { value: 'No', score: 0, additional: true }
                ],
                userAnswer: { value: null, score: null, additional: false },
                additionalQuestion: {
                    title: 'If No, Select Reason',
                    answers: [
                        { value: 'Store issue', score: 1 },
                        { value: 'Field Staff Error', score: 0 }
                    ],
                    userAnswer: { value: null, score: null }
                }
            },
            {
                title: 'Do you have any adhoc Blitz end?',
                type: 'photo',
                answers: [
                    { value: 'Yes', score: 1, additional: true },
                    { value: 'No', score: 0, additional: false }
                ],
                userAnswer: { value: null, score: null, additional: false },
                additionalQuestion: {
                    title: 'If Yes, Take a Photo',
                    answers: null,
                    userAnswer: { value: null, score: null }
                }
            },
            {
                title: 'Is forward share as per Rate of Sale?',
                type: 'select',
                answers: [
                    { value: 'Yes', score: 1, additional: false },
                    { value: 'No', score: 0, additional: true }
                ],
                userAnswer: { value: null, score: null, additional: false },
                additionalQuestion: {
                    title: 'If No, Select Reason',
                    answers: [
                        { value: 'PNP PLANO', score: 1 },
                        { value: 'SPAR PLANO', score: 0 },
                        { value: 'Field Staff Error', score: 0 },
                        { value: 'MMS/ROS/DEMAND', score: 1 }
                    ],
                    userAnswer: { value: null, score: null }
                }
            },
        ];
    }

    takePhoto()
    {
        this.alertProvider.showMessage('Not enabled.');
    }

    userAnswerChanged()
    {
        this.scoreAmount = 0;

        for (let i = 0; i < this.scoreCard.length; i++)
        {
            const scoreCard = this.scoreCard[i];

            if (scoreCard.questions && scoreCard.questions.length > 0)
            {
                for (let j = 0; j < scoreCard.questions.length; j++)
                {
                    const question = scoreCard.questions[j];

                    if (question.userAnswer && question.userAnswer.score)
                    {
                        this.scoreAmount += question.userAnswer.score;
                    }

                    if (question.additionalQuestion && question.additionalQuestion.userAnswer && question.additionalQuestion.userAnswer.score)
                    {
                        this.scoreAmount += question.additionalQuestion.userAnswer.score;
                    }
                }
            }
        }

        for (let i = 0; i < this.additionalQuestions.length; i++)
        {
            const question = this.additionalQuestions[i];

            if (question.userAnswer && question.userAnswer.score)
            {
                this.scoreAmount += question.userAnswer.score;
            }

            if (question.additionalQuestion && question.additionalQuestion.userAnswer && question.additionalQuestion.userAnswer.score)
            {
                this.scoreAmount += question.additionalQuestion.userAnswer.score;
            }
        }
    }

    selectProduct()
    {
        if (!this.filteredProducts || this.filteredProducts.length < 1)
        {
            this.alertProvider.showMessage('Products not found.');

            return;
        }

        const inputs = [];

        for (let i = 0; i < this.filteredProducts.length; i++)
        {
            inputs[i] = { type: 'radio', label: this.filteredProducts[i].name, value: this.filteredProducts[i] };
        }

        this.alertCtrl.create({
            title: 'Select a Product',
            inputs: inputs,
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel'
                },
                {
                    text: 'Ok',
                    handler: (product: ProductModel) =>
                    {
                        if (product)
                        {
                            this.selectedProduct = product;
                            this.filterProducts();
                        }
                        else
                        {
                            this.alertProvider.showMessage('Product not found.');
                        }
                    }
                }
            ]
        }).present();
    }

    private filterProducts()
    {
        if (this.scoreCard && this.scoreCard.length > 0)
        {
            this.filteredProducts = this.allProducts.filter((ap) => !this.scoreCard.some((sc) => sc.product.id === ap.id));
        }
    }

    nextProduct()
    {
        if (!this.scoreCard)
        {
            this.scoreCard = [{ store: this.store, product: this.selectedProduct, questions: this.questions }];
        }

        this.selectedProduct = this.filteredProducts.find((product) => product.id !== this.selectedProduct.id);
        this.setupQuestions();
        this.filterProducts();

        this.content.scrollToTop();
    }

    openSignPage()
    {
        const data = {
            scoreCards: JSON.stringify(this.scoreCard),
            additionalQuestions: JSON.stringify(this.additionalQuestions)
        };
        this.navCtrl.push('ScoreCardSignPage', { data: data });
    }
}

