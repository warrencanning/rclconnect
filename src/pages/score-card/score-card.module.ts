import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CommonModule } from '@angular/common';

import { ScoreCardPage } from './score-card';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
    declarations: [ScoreCardPage],
    imports: [CommonModule, ComponentsModule, IonicPageModule.forChild(ScoreCardPage)],
})
export class ScoreCardPageModule
{
}
