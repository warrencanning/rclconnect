import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, NavParams } from 'ionic-angular';
import { finalize } from 'rxjs/operators';

import * as moment from 'moment';

import { Geolocation, Geoposition } from '@ionic-native/geolocation';

import { CallModel, CreateDoneCallModel, StoreModel } from '../../models';

import {
    AlertProvider, LoadingProvider, StoreProvider
} from '../../providers';

@IonicPage()
@Component({
    selector: 'page-store',
    templateUrl: 'store.html',
})
export class StorePage
{
    store: StoreModel;
    createDoneCallModel = new CreateDoneCallModel();
    isCheckedIn = false; // TODO: change to false in prod

    constructor(private navCtrl: NavController,
                private navParams: NavParams,
                private storeProvider: StoreProvider,
                private loadingProvider: LoadingProvider,
                private alertProvider: AlertProvider,
                private geolocation: Geolocation,
                private modalCtrl: ModalController)
    {
    }

    ionViewDidLoad()
    {
        this.getCurrentPosition();
        this.store = this.navParams.get('store');
    }

    private getCurrentPosition()
    {
        this.geolocation.getCurrentPosition()
            .then((resp: Geoposition) =>
            {
                this.createDoneCallModel.latitude = resp.coords.latitude;
                this.createDoneCallModel.longitude = resp.coords.longitude;
            })
            .catch((error) =>
            {
                this.alertProvider.putErrorFromResponseToConsole('getCurrentPosition', 'StorePage', error);
            });
    }

    isUserInStore(): boolean
    {
        if (this.isCheckedIn)
        {
            return true;
        }

        return false;
    }

    openEditCustomerPage()
    {
        const editStoreModal = this.modalCtrl.create('EditStorePage', {
            store: this.store
        });
        editStoreModal.onDidDismiss((store: StoreModel) =>
        {
            if (store)
            {
                this.store = store;
            }
        });
        editStoreModal.present();
    }

    checkIn()
    {
        this.createDoneCallModel.check_in = moment().format('YYYY-MM-DD HH:mm:ss');
        this.createDoneCallModel.store_call_id = this.store.call.id;
        this.isCheckedIn = true;
    }

    checkOut()
    {
        this.loadingProvider.show();
        this.createDoneCallModel.check_out = moment().format('YYYY-MM-DD HH:mm:ss');
        this.isCheckedIn = false;

        this.storeProvider.createCall(this.createDoneCallModel)
            .pipe(finalize(() => this.loadingProvider.hide()))
            .subscribe((call: CallModel) =>
            {
                this.store.call = call;
                this.navCtrl.pop();
            }, (error) =>
            {
                this.alertProvider.putErrorFromResponseToConsole('checkOut', 'StorePage', error);
                this.alertProvider.showErrorFromResponse(error);
            });
    }

    openObjectivesNotes()
    {
        this.navCtrl.push('NotesPage', { store: this.store });
    }

    openScoreCardPage()
    {
        this.navCtrl.push('ScoreCardPage', { store: this.store });
    }

    openPromotionalExecutionPage()
    {
        this.navCtrl.push('PromotionExecutionPage', { store: this.store });
    }

    openSurveysPage()
    {
        this.alertProvider.showMessage('Not Enabled.');
        // this.navCtrl.push('SurveysPage');
    }

    openForwardSharePage()
    {
        this.navCtrl.push('ForwardSharePage', { store: this.store });
    }

    openPlanogramPage()
    {
        this.navCtrl.push('PlanogramPage', { store: this.store });
    }

    openStockOnHandPage()
    {
        this.alertProvider.showMessage('Not Enabled.');
        // this.navCtrl.push('StockOnHandPage');
    }

    openOrderPage()
    {
        this.alertProvider.showMessage('Not Enabled.');
    }

    openSalesPage()
    {
        this.navCtrl.push('SalesDataPage', { store: this.store });
    }
}
