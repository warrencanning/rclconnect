import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';
import { finalize } from 'rxjs/operators';

import { CreateStoreNoteModel, StoreModel, StoreNoteModel } from '../../models';

import { AlertProvider, LoadingProvider, StoreProvider } from '../../providers';

@IonicPage()
@Component({
    selector: 'page-new-note',
    templateUrl: 'new-note.html',
})
export class NewNotePage
{
    createNoteModel = new CreateStoreNoteModel();
    store: StoreModel;

    constructor(private navParams: NavParams,
                private storeProvider: StoreProvider,
                private alertProvider: AlertProvider,
                private loadingProvider: LoadingProvider,
                private viewCtrl: ViewController)
    {
    }

    ionViewDidLoad()
    {
        this.store = this.navParams.get('store');
        this.createNoteModel.store_id = this.store.id;
    }

    isDataValid(): boolean
    {
        if (!this.createNoteModel.store_id)
        {
            return false;
        }

        if (!this.createNoteModel.note)
        {
            return false;
        }

        return true;
    }

    save()
    {
        this.loadingProvider.show();
        this.storeProvider.createNote(this.createNoteModel)
            .pipe(finalize(() => this.loadingProvider.hide()))
            .subscribe((note: StoreNoteModel) =>
            {
                this.dismiss(note);
            }, (error) =>
            {
                this.alertProvider.putErrorFromResponseToConsole('save', 'NewNotePage', error);
                this.alertProvider.showErrorFromResponse(error);
            });
    }

    dismiss(note: StoreNoteModel = null)
    {
        if (note)
        {
            this.viewCtrl.dismiss(note);
        }
        else
        {
            this.viewCtrl.dismiss(null);
        }
    }
}
