import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { NewNotePage } from './new-note';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
    declarations: [NewNotePage],
    imports: [ComponentsModule, IonicPageModule.forChild(NewNotePage)],
})
export class NewNotePageModule
{
}
