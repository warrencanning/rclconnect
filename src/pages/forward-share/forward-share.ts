import { ChangeDetectorRef, Component } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular';

import { ProductCategoryModel, ProductSubCategoryModel, CreateForwardShareModel } from '../../models';

import { AlertProvider, LocalStorageProvider } from '../../providers';

@IonicPage()
@Component({
    selector: 'page-forward-share',
    templateUrl: 'forward-share.html',
})
export class ForwardSharePage
{
    createForwardShareModel = new CreateForwardShareModel();
    selectedCategory: ProductCategoryModel;
    selectedSubCategory: ProductSubCategoryModel;

    private categories: ProductCategoryModel[];

    constructor(private navCtrl: NavController,
                private navParams: NavParams,
                private changeDetectorRef: ChangeDetectorRef,
                private alertProvider: AlertProvider,
                private alertCtrl: AlertController)
    {
    }

    ionViewDidLoad()
    {
        this.categories = LocalStorageProvider.getProductCategories();
        this.createForwardShareModel.store_id = this.navParams.get('store').id;
    }

    selectCategory()
    {
        if (!this.categories || this.categories.length < 1)
        {
            this.alertProvider.showMessage('Categories not found.');

            return;
        }

        const inputs = [];

        for (let i = 0; i < this.categories.length; i++)
        {
            inputs[i] = { type: 'radio', label: this.categories[i].category, value: this.categories[i] };
        }

        this.alertCtrl.create({
            title: 'Select a Category',
            inputs: inputs,
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel'
                },
                {
                    text: 'Ok',
                    handler: (category: ProductCategoryModel) =>
                    {
                        if (category)
                        {
                            this.selectedCategory = category;
                        }
                        else
                        {
                            this.alertProvider.showMessage('Category not found.');
                        }
                    }
                }
            ]
        }).present();
    }

    selectSubCategory()
    {
        if (this.selectedCategory.sub_categories.length < 1)
        {
            this.alertProvider.showMessage('Sub Categories not found.');

            return;
        }

        const inputs = [];

        for (let i = 0; i < this.selectedCategory.sub_categories.length; i++)
        {
            inputs[i] = {
                type: 'radio',
                label: this.selectedCategory.sub_categories[i].sub_category,
                value: this.selectedCategory.sub_categories[i]
            };
        }

        this.alertCtrl.create({
            title: 'Choose a Sub Category',
            inputs: inputs,
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel'
                },
                {
                    text: 'Ok',
                    handler: (subCategory: ProductSubCategoryModel) =>
                    {
                        if (subCategory)
                        {
                            this.selectedSubCategory = subCategory;
                            this.createForwardShareModel.product_sub_category_id = this.selectedSubCategory.id;
                        }
                        else
                        {
                            this.alertProvider.showMessage('Sub Category not found.');
                        }
                    }
                }
            ]
        }).present();
    }

    categoryFacingChanged(categoryFacings: any)
    {
        if (!categoryFacings)
        {
            return;
        }

        categoryFacings = parseFloat(categoryFacings);
        this.createForwardShareModel.category_facings = categoryFacings;
        this.createForwardShareModel.category_facings_percent = this.getPercent(categoryFacings);

        this.changeDetectorRef.detectChanges();

        const categoryElement = document.getElementById('category-facing');
        const competitorElement = document.getElementById('competitor-facing');
        const additionalElement = document.getElementById('additional-value');
        const competitorFacings = this.createForwardShareModel.competitor_facings;

        if (!competitorFacings || competitorFacings < 0)
        {
            categoryElement.style.width = '100%';
            // additionalElement.style.width = '100%';
            competitorElement.style.width = '0';
        }
        else
        {
            this.createForwardShareModel.competitor_facings_percent = this.getPercent(competitorFacings);
            categoryElement.style.width = `${this.createForwardShareModel.category_facings_percent}%`;
            competitorElement.style.width = `${this.createForwardShareModel.competitor_facings_percent}%`;
            this.createForwardShareModel.target = 100 - this.createForwardShareModel.competitor_facings_percent;
            // additionalElement.style.width = `${this.createForwardShareModel.target}%`;
        }
    }

    competitorFacingChange(competitorFacings: any)
    {
        if (!competitorFacings)
        {
            return;
        }

        competitorFacings = parseFloat(competitorFacings);
        this.createForwardShareModel.competitor_facings = parseFloat(competitorFacings);
        this.createForwardShareModel.competitor_facings_percent = this.getPercent(competitorFacings);

        this.changeDetectorRef.detectChanges();

        const categoryElement = document.getElementById('category-facing');
        const competitorElement = document.getElementById('competitor-facing');
        const additionalElement = document.getElementById('additional-value');
        const categoryFacings = this.createForwardShareModel.category_facings;

        if (!categoryFacings || categoryFacings < 0)
        {
            competitorElement.style.width = '100%';
            categoryElement.style.width = '0';
            // additionalElement.style.width = '0';
        }
        else
        {
            this.createForwardShareModel.category_facings_percent = this.getPercent(categoryFacings);
            categoryElement.style.width = `${this.createForwardShareModel.category_facings_percent}%`;
            competitorElement.style.width = `${this.createForwardShareModel.competitor_facings_percent}%`;
            this.createForwardShareModel.target = 100 - this.createForwardShareModel.competitor_facings_percent;
            // additionalElement.style.width = `${this.createForwardShareModel.target}%`;
        }
    }

    private getPercent(facings: number): number
    {
        const categoryFacings = this.createForwardShareModel.category_facings ? this.createForwardShareModel.category_facings : 0;
        const competitorFacings = this.createForwardShareModel.competitor_facings ? this.createForwardShareModel.competitor_facings : 0;
        const total = categoryFacings + competitorFacings;
        const value = (facings / total * 100).toFixed(0);

        return parseFloat(value);
    }

    isModelValid(): boolean
    {
        if (!this.createForwardShareModel.product_sub_category_id)
        {
            return false;
        }

        if (!this.createForwardShareModel.store_id)
        {
            return false;
        }

        if (!this.createForwardShareModel.category_facings)
        {
            return false;
        }

        return true;
    }

    openSignPage()
    {
        this.navCtrl.push('ForwardShareSignPage', { createForwardShareModel: this.createForwardShareModel });
    }
}
