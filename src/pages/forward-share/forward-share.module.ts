import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { ForwardSharePage } from './forward-share';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
    declarations: [ForwardSharePage],
    imports: [ComponentsModule, IonicPageModule.forChild(ForwardSharePage)],
})
export class ForwardSharePageModule
{
}
