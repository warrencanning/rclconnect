import { Component } from '@angular/core';
import { IonicPage, ModalController, NavParams } from 'ionic-angular';

import { StoreNoteModel } from '../../models';

import { LocalStorageProvider } from '../../providers';

@IonicPage()
@Component({
    selector: 'page-notes',
    templateUrl: 'notes.html',
})
export class NotesPage
{
    lastNote: StoreNoteModel;

    constructor(private navParams: NavParams,
                private modalCtrl: ModalController)
    {
    }

    ionViewDidLoad()
    {
        this.lastNote = LocalStorageProvider.getLastStoreNote();
    }

    makeNewNote()
    {
        const newNoteModal = this.modalCtrl.create('NewNotePage', { store: this.navParams.get('store') });
        newNoteModal.onDidDismiss((lastNote: StoreNoteModel) =>
        {
            if (lastNote)
            {
                this.lastNote = lastNote;
                LocalStorageProvider.setLastStoreNote(this.lastNote);
            }
        });
        newNoteModal.present();
    }
}
