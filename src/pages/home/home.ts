import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { finalize } from 'rxjs/operators';
import { forkJoin } from 'rxjs/observable/forkJoin';

import {
    AlertProvider,
    AuthProvider,
    LoadingProvider,
    LocalStorageProvider,
    ProductProvider,
    PromotionProvider,
    QuestionnaireProvider,
    StoreProvider,
    UserProvider
} from '../../providers';

import { DashboardModel, StoreModel } from '../../models';

@IonicPage()
@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage
{
    dashboardData: DashboardModel;

    private stores: StoreModel[];

    constructor(private navCtrl: NavController,
                private loadingProvider: LoadingProvider,
                private authProvider: AuthProvider,
                private userProvider: UserProvider,
                private storeProvider: StoreProvider,
                private alertProvider: AlertProvider,
                private questionnaireProvider: QuestionnaireProvider,
                private productProvider: ProductProvider,
                private promotionProvider: PromotionProvider)
    {
    }

    ionViewDidLoad()
    {
        this.getData();
    }

    ionViewDidEnter()
    {
        this.authProvider.userWasLoggedIn();
    }

    private getData()
    {
        this.loadingProvider.show();
        forkJoin(
            this.userProvider.getDashboardData(),
            this.storeProvider.getStores(),
            this.storeProvider.getStoreTypes(),
            this.storeProvider.getLastNote(),
            this.productProvider.getProductCategories(),
            this.productProvider.getProducts(),
            this.questionnaireProvider.getQuestionnaire(),
            this.promotionProvider.getPromotions()
        )
            .pipe(finalize(() => this.loadingProvider.hide()))
            .subscribe(([dashboardData, stores, storeTypes, note, productCategories, products, questionnaire, promotions]) =>
            {
                this.dashboardData = dashboardData;
                this.stores = stores;
                LocalStorageProvider.setStoreTypes(storeTypes);
                LocalStorageProvider.setLastStoreNote(note);
                LocalStorageProvider.setProductCategories(productCategories);
                LocalStorageProvider.setProducts(products);
                LocalStorageProvider.setQuestionnaire(questionnaire);
                LocalStorageProvider.setPromotions(promotions);
            }, (error) =>
            {
                this.alertProvider.putErrorFromResponseToConsole('getData', 'HomePage', error);
                this.alertProvider.showErrorFromResponse(error);
            });
    }

    openProfilePage()
    {
        this.alertProvider.showMessage('Not Enabled.');
        //     this.navCtrl.push('ProfilePage');
    }

    quickStart()
    {
        if (this.isCanOpen())
        {
            this.navCtrl.push('StoresPage', { stores: this.stores });
        }
    }

    openCallsPage()
    {
        if (this.isCanOpen())
        {
            this.navCtrl.push('CallsPage', { stores: this.stores });
        }
    }

    private isCanOpen(): boolean
    {
        if (this.stores && this.stores.length > 0)
        {
            return true;
        }

        this.alertProvider.showMessage('No stores.');

        return false;
    }
}
