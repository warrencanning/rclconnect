import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ProductCategoryModel } from '../../models';

import { LocalStorageProvider } from '../../providers';

@IonicPage()
@Component({
    selector: 'page-planogram',
    templateUrl: 'planogram.html',
})
export class PlanogramPage
{
    categories: ProductCategoryModel[];

    constructor(private navCtrl: NavController,
                private navParams: NavParams)
    {
    }

    ionViewDidLoad()
    {
        this.categories = LocalStorageProvider.getProductCategories();
    }

    openPlanogramCapturePage(category: ProductCategoryModel)
    {
        this.navCtrl.push('PlanogramCapturePage',
            { category: category, store: this.navParams.get('store') }
        );
    }
}
