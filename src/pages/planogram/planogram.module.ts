import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { PlanogramPage } from './planogram';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
    declarations: [PlanogramPage],
    imports: [ComponentsModule, IonicPageModule.forChild(PlanogramPage)],
})
export class PlanogramPageModule
{
}
