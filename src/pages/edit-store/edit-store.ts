import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';
import { finalize } from 'rxjs/operators';

import { CreateStoreContactModel, StoreContactModel, StoreModel, StoreTypeModel } from '../../models';

import { AlertProvider, LoadingProvider, LocalStorageProvider, StoreProvider } from '../../providers';

@IonicPage()
@Component({
    selector: 'page-edit-store',
    templateUrl: 'edit-store.html',
})
export class EditStorePage
{
    store: StoreModel;
    storeTypes: StoreTypeModel[];
    daysInWeek = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    createStoreContactModel: CreateStoreContactModel;

    constructor(private navParams: NavParams,
                private viewCtrl: ViewController,
                private storeProvider: StoreProvider,
                private alertProvider: AlertProvider,
                private loadingProvider: LoadingProvider)
    {
    }

    ionViewDidLoad()
    {
        this.storeTypes = LocalStorageProvider.getStoreTypes();
        this.store = this.navParams.get('store');

        if (!this.store.contact)
        {
            this.createStoreContactModel = new CreateStoreContactModel(this.store.id);
        }
    }

    isDataValid(): boolean
    {
        if (!this.store.type)
        {
            return false;
        }

        if (!this.store.name)
        {
            return false;
        }

        return true;
    }

    update()
    {
        this.loadingProvider.show();

        if (!this.store.contact)
        {
            this.storeProvider.createStoreContact(this.createStoreContactModel)
                .subscribe((contact: StoreContactModel) =>
                {
                    this.store.contact = contact;
                    this.updateStore();
                }, (error) =>
                {
                    this.loadingProvider.hide();
                    this.alertProvider.putErrorFromResponseToConsole('update', 'EditStorePage', error);
                    this.alertProvider.showErrorFromResponse(error);
                });
        }
        else
        {
            this.updateStore();
        }
    }

    private updateStore()
    {
        this.storeProvider.update(this.store)
            .pipe(finalize(() => this.loadingProvider.hide()))
            .subscribe((store: StoreModel) =>
            {
                this.alertProvider.showMessage('Store was updated.');
                this.store = store;
                this.dismiss(true);
            }, (error) =>
            {
                this.alertProvider.putErrorFromResponseToConsole('updateStore', 'EditStorePage', error);
                this.alertProvider.showErrorFromResponse(error);
            });
    }

    dismiss(isNeedToUpdateStore = false)
    {
        if (isNeedToUpdateStore)
        {
            this.viewCtrl.dismiss(this.store);
        }
        else
        {
            this.viewCtrl.dismiss(null);
        }
    }
}
