import { Component } from '@angular/core';
import { ActionSheetController, AlertController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { finalize } from 'rxjs/operators';

import { Camera, CameraOptions } from '@ionic-native/camera';

import {
    CreatePromotionExecutionModel,
    ProductCategoryModel,
    PromotionExecutionModel,
    PromotionModel
} from '../../models';

import {
    AlertProvider,
    FileTransferProvider,
    LoadingProvider,
    LocalStorageProvider,
    PromotionProvider
} from '../../providers';

@IonicPage()
@Component({
    selector: 'page-promotion-execution',
    templateUrl: 'promotion-execution.html',
})
export class PromotionExecutionPage
{
    selectedCategory: ProductCategoryModel;
    selectedPromotion: PromotionModel;
    createPromotionExecutionModel = new CreatePromotionExecutionModel();

    private categories: ProductCategoryModel[];
    private promotions: PromotionModel[];

    constructor(private navCtrl: NavController,
                private navParams: NavParams,
                private camera: Camera,
                private actionSheetCtrl: ActionSheetController,
                private alertProvider: AlertProvider,
                private loadingProvider: LoadingProvider,
                private fileTransferProvider: FileTransferProvider,
                private alertCtrl: AlertController,
                private promotionProvider: PromotionProvider)
    {
    }

    ionViewDidLoad()
    {
        this.categories = LocalStorageProvider.getProductCategories();
        this.promotions = LocalStorageProvider.getPromotions();
        this.createPromotionExecutionModel.store_id = this.navParams.get('store').id;
    }

    selectCategory()
    {
        if (!this.categories || this.categories.length < 1)
        {
            this.alertProvider.showMessage('Categories not found.');

            return;
        }

        const inputs = [];

        for (let i = 0; i < this.categories.length; i++)
        {
            inputs[i] = { type: 'radio', label: this.categories[i].category, value: this.categories[i] };
        }

        this.alertCtrl.create({
            title: 'Select a Category',
            inputs: inputs,
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel'
                },
                {
                    text: 'Ok',
                    handler: (category: ProductCategoryModel) =>
                    {
                        if (category)
                        {
                            this.selectedCategory = category;
                            this.createPromotionExecutionModel.product_category_id = this.selectedCategory.id;
                        }
                        else
                        {
                            this.alertProvider.showMessage('Category not found.');
                        }
                    }
                }
            ]
        }).present();
    }

    selectPromotion()
    {
        if (!this.promotions || this.promotions.length < 1)
        {
            this.alertProvider.showMessage('Promotions not found.');

            return;
        }

        const inputs = [];

        for (let i = 0; i < this.promotions.length; i++)
        {
            inputs[i] = { type: 'radio', label: this.promotions[i].title, value: this.promotions[i] };
        }

        this.alertCtrl.create({
            title: 'Select a Promotion',
            inputs: inputs,
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel'
                },
                {
                    text: 'Ok',
                    handler: (promotion: PromotionModel) =>
                    {
                        if (promotion)
                        {
                            this.selectedPromotion = promotion;
                            this.createPromotionExecutionModel.promotion_id = this.selectedPromotion.id;
                        }
                        else
                        {
                            this.alertProvider.showMessage('Promotion not found.');
                        }
                    }
                }
            ]
        }).present();
    }

    executionChanged()
    {
        if (this.createPromotionExecutionModel.execution && this.createPromotionExecutionModel.execution.toLowerCase() === 'yes')
        {
            this.createPromotionExecutionModel.no_reason = null;
        }
    }

    choosePhotoSource()
    {
        this.actionSheetCtrl.create({
            title: 'Select Image Source',
            buttons: [
                {
                    text: 'Load from Library',
                    icon: 'image',
                    handler: () =>
                    {
                        this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Use Camera',
                    icon: 'camera',
                    handler: () =>
                    {
                        this.takePicture(this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        }).present();
    }

    private takePicture(sourceType: number)
    {
        const options: CameraOptions = {
            quality: 90,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: sourceType,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };

        this.camera.getPicture(options)
            .then((imgUri: string) =>
            {
                this.fileTransferProvider.makeFileIntoBlob(imgUri)
                    .then((data: { fileName: string, file: Blob }) =>
                    {
                        this.createPromotionExecutionModel.images[this.createPromotionExecutionModel.images.length] = data;
                    })
                    .catch((error) =>
                    {
                        this.alertProvider.putErrorFromResponseToConsole('takePicture', 'PromotionalExecutionPage', error);
                    });
            })
            .catch((error) =>
            {
                this.alertProvider.putErrorFromResponseToConsole('takePicture', 'PromotionalExecutionPage', error);
            });
    }

    isModelValid(): boolean
    {
        if (!this.createPromotionExecutionModel.execution)
        {
            return false;
        }

        if (this.createPromotionExecutionModel.execution.toLowerCase() === 'no' && !this.createPromotionExecutionModel.no_reason)
        {
            return false;
        }

        if (!this.createPromotionExecutionModel.product_category_id)
        {
            return false;
        }

        if (!this.createPromotionExecutionModel.promotion_id)
        {
            return false;
        }

        if (!this.createPromotionExecutionModel.store_id)
        {
            return false;
        }

        return true;
    }

    submit()
    {
        this.loadingProvider.show();
        this.promotionProvider.createPromotionExecution(this.createPromotionExecutionModel)
            .subscribe((promotionExecution: PromotionExecutionModel) =>
            {
                if (this.createPromotionExecutionModel.images.length > 0)
                {
                    const savePromotionExecutionImagesUrl = `/v1/promotions/executions/${promotionExecution.id}/files`;
                    this.fileTransferProvider.sendFiles(this.createPromotionExecutionModel.images, savePromotionExecutionImagesUrl)
                        .pipe(finalize(() => this.loadingProvider.hide()))
                        .subscribe(() =>
                        {
                            this.alertProvider.showMessage('Saved.');
                            this.navCtrl.pop();
                        }, (error) =>
                        {
                            this.alertProvider.putErrorFromResponseToConsole('submit', 'PromotionalExecutionPage', error);
                            this.alertProvider.showErrorFromResponse(error);
                        });
                }
                else
                {
                    this.loadingProvider.hide();
                    this.alertProvider.showMessage('Saved.');
                    this.navCtrl.pop();
                }
            }, (error) =>
            {
                this.loadingProvider.hide();
                this.alertProvider.putErrorFromResponseToConsole('submit', 'PromotionalExecutionPage', error);
                this.alertProvider.showErrorFromResponse(error);
            });
    }
}
