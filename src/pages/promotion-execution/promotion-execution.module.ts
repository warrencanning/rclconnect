import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { PromotionExecutionPage } from './promotion-execution';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
    declarations: [PromotionExecutionPage],
    imports: [ComponentsModule, IonicPageModule.forChild(PromotionExecutionPage)],
})
export class PromotionExecutionPageModule
{
}
