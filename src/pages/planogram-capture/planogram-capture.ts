import { Component } from '@angular/core';
import { ActionSheetController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { finalize } from 'rxjs/operators';

import { Camera, CameraOptions } from '@ionic-native/camera';

import { CreatePlanogramModel, PlanogramModel, ProductCategoryModel } from '../../models';

import { AlertProvider, FileTransferProvider, LoadingProvider, PlanogramProvider } from '../../providers';

@IonicPage()
@Component({
    selector: 'page-planogram-capture',
    templateUrl: 'planogram-capture.html',
})
export class PlanogramCapturePage
{
    category: ProductCategoryModel;
    createPlanogramModel = new CreatePlanogramModel();

    constructor(private navCtrl: NavController,
                private navParams: NavParams,
                private camera: Camera,
                private alertProvider: AlertProvider,
                private fileTransferProvider: FileTransferProvider,
                private planogramProvider: PlanogramProvider,
                private loadingProvider: LoadingProvider,
                private actionSheetCtrl: ActionSheetController)
    {
    }

    ionViewDidLoad()
    {
        this.category = this.navParams.get('category');
        this.createPlanogramModel.product_category_id = this.category.id;
        this.createPlanogramModel.store_id = this.navParams.get('store').id;
    }

    viewPlanogram()
    {
        if (this.category.category.toLowerCase() === 'frozen fish')
        {
            this.navCtrl.push('ImageViewerPage');
        }
        else
        {
            this.alertProvider.showMessage('Planograms not found.');
        }
    }

    accurateChanged()
    {
        if (this.createPlanogramModel.accurate && this.createPlanogramModel.accurate.toLowerCase() === 'yes')
        {
            this.createPlanogramModel.no_reason = null;
        }
    }

    choosePhotoSource()
    {
        this.actionSheetCtrl.create({
            title: 'Select Image Source',
            buttons: [
                {
                    text: 'Load from Library',
                    icon: 'image',
                    handler: () =>
                    {
                        this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Use Camera',
                    icon: 'camera',
                    handler: () =>
                    {
                        this.takePicture(this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        }).present();
    }

    private takePicture(sourceType: number)
    {
        const options: CameraOptions = {
            quality: 90,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: sourceType,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };

        this.camera.getPicture(options)
            .then((imgUri: string) =>
            {
                this.fileTransferProvider.makeFileIntoBlob(imgUri)
                    .then((data: { fileName: string, file: Blob }) =>
                    {
                        this.createPlanogramModel.images[this.createPlanogramModel.images.length] = data;
                    })
                    .catch((error) =>
                    {
                        this.alertProvider.putErrorFromResponseToConsole('takePicture', 'PlanogramCapturePage', error);
                    });
            })
            .catch((error) =>
            {
                this.alertProvider.putErrorFromResponseToConsole('takePicture', 'PlanogramCapturePage', error);
            });
    }

    isModelValid(): boolean
    {
        if (!this.createPlanogramModel.accurate)
        {
            return false;
        }

        if (this.createPlanogramModel.accurate.toLowerCase() === 'no' && !this.createPlanogramModel.no_reason)
        {
            return false;
        }

        if (!this.createPlanogramModel.store_id)
        {
            return false;
        }

        if (!this.createPlanogramModel.product_category_id)
        {
            return false;
        }

        return true;
    }

    submit()
    {
        this.loadingProvider.show();
        this.planogramProvider.createPlanogram(this.createPlanogramModel)
            .subscribe((planogram: PlanogramModel) =>
            {
                if (this.createPlanogramModel.images.length > 0)
                {
                    const savePromotionExecutionImagesUrl = `/v1/planograms/${planogram.id}/files`;
                    this.fileTransferProvider.sendFiles(this.createPlanogramModel.images, savePromotionExecutionImagesUrl)
                        .pipe(finalize(() => this.loadingProvider.hide()))
                        .subscribe(() =>
                        {
                            this.alertProvider.showMessage('Saved.');
                            this.navCtrl.pop();
                        }, (error) =>
                        {
                            this.alertProvider.putErrorFromResponseToConsole('submit', 'PlanogramCapturePage', error);
                            this.alertProvider.showErrorFromResponse(error);
                        });
                }
                else
                {
                    this.loadingProvider.hide();
                    this.alertProvider.showMessage('Saved.');
                    this.navCtrl.pop();
                }
            }, (error) =>
            {
                this.loadingProvider.hide();
                this.alertProvider.putErrorFromResponseToConsole('submit', 'PlanogramCapturePage', error);
                this.alertProvider.showErrorFromResponse(error);
            });
    }
}
