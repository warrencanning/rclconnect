import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { PlanogramCapturePage } from './planogram-capture';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
    declarations: [PlanogramCapturePage],
    imports: [ComponentsModule, IonicPageModule.forChild(PlanogramCapturePage)],
})
export class PlanogramCapturePageModule
{
}
