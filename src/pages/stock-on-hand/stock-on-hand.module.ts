import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StockOnHandPage } from './stock-on-hand';

@NgModule({
    declarations: [
        StockOnHandPage,
    ],
    imports: [
        IonicPageModule.forChild(StockOnHandPage),
    ],
})
export class StockOnHandPageModule
{
}
