import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the StockOnHandPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-stock-on-hand',
    templateUrl: 'stock-on-hand.html',
})
export class StockOnHandPage
{

    constructor(public navCtrl: NavController, public navParams: NavParams)
    {
    }

    ionViewDidLoad()
    {
        console.log('ionViewDidLoad StockOnHandPage');
    }

}
