import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { SignaturePadModule } from 'angular2-signaturepad';

import { ScoreCardSignPage } from './score-card-sign';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
    declarations: [ScoreCardSignPage],
    imports: [ComponentsModule, SignaturePadModule, IonicPageModule.forChild(ScoreCardSignPage)],
})
export class ScoreCardSignPageModule
{
}
