import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { SignaturePad } from 'angular2-signaturepad/signature-pad';

import { AlertProvider, LoadingProvider, QuestionnaireProvider } from '../../providers';
import { finalize } from 'rxjs/operators';

@IonicPage()
@Component({
    selector: 'page-score-card-sign',
    templateUrl: 'score-card-sign.html',
})
export class ScoreCardSignPage
{
    @ViewChild(SignaturePad) signaturePad: SignaturePad;

    signaturePadOptions = { // passed through to szimek/signature_pad constructor
        'minWidth': 1,
        'canvasWidth': 500,
        'canvasHeight': 300
    };

    private signature: Blob;

    constructor(private navCtrl: NavController,
                private navParams: NavParams,
                private loadingProvider: LoadingProvider,
                private alertProvider: AlertProvider,
                private questionnaireProvider: QuestionnaireProvider)
    {
    }

    ionViewDidLoad()
    {
    }

    ionViewDidEnter()
    {
        /**
         * this.signaturePad is now available
         * we can set options like this:
         * this.signaturePad.set('minWidth', 2); set szimek/signature_pad options at runtime
         * or invoke functions:
         * this.signaturePad.clear(); from szimek/signature_pad API
         */
        this.signaturePad.set('minWidth', 1);
        this.clearSignPad();
    }

    clearSignPad()
    {
        this.signaturePad.clear();
        this.signature = null;
    }

    drawComplete()
    {
        this.signature = this.base64toBlob(this.signaturePad.toDataURL());
    }

    private base64toBlob(dataURI: string): Blob
    {
        const byteString = atob(dataURI.split(',')[1]);
        const ab = new ArrayBuffer(byteString.length);
        let ia = new Uint8Array(ab);

        for (let i = 0; i < byteString.length; i++)
        {
            ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ab], { type: 'image/jpeg' });
    }

    done()
    {
        this.loadingProvider.show();
        const data = this.navParams.get('data');
        // TODO: decide with model and add signature
        this.questionnaireProvider.createScoreCard(data)
            .pipe(finalize(() => this.loadingProvider.hide()))
            .subscribe(() =>
            {
                /**
                 * popTo StorePage that way because popTo('StorePage') not working
                 */
                this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - 3));
            }, (error) =>
            {
                this.alertProvider.putErrorFromResponseToConsole('done', 'ScoreCardSignPage', error);
                this.alertProvider.showErrorFromResponse(error);
            });
    }
}
