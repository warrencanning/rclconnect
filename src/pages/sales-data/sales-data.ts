import { Component } from '@angular/core';
import { AlertController, IonicPage, NavParams } from 'ionic-angular';

import { ProductCategoryModel, ProductSubCategoryModel, SaleModel } from '../../models';

import { AlertProvider, LoadingProvider, LocalStorageProvider, StoreProvider } from '../../providers';
import { finalize } from 'rxjs/operators';

@IonicPage()
@Component({
    selector: 'page-sales-data',
    templateUrl: 'sales-data.html',
})
export class SalesDataPage
{
    selectedCategory: ProductCategoryModel;
    selectedSubCategory: ProductSubCategoryModel;
    data: SaleModel;

    private categories: ProductCategoryModel[];

    constructor(private navParams: NavParams,
                private alertProvider: AlertProvider,
                private alertCtrl: AlertController,
                private storeProvider: StoreProvider,
                private loadingProvider: LoadingProvider)
    {
    }

    ionViewDidLoad()
    {
        this.loadingProvider.show();
        this.getSaleData();
        this.categories = LocalStorageProvider.getProductCategories();
    }

    private getSaleData()
    {
        this.storeProvider.getSalesData(this.navParams.get('store').id)
            .pipe(finalize(() => this.loadingProvider.hide()))
            .subscribe((data) =>
            {
                this.data = data;
            }, (error) =>
            {
                this.alertProvider.putErrorFromResponseToConsole('getSaleData', 'SalesDataPage', error);
                this.alertProvider.showErrorFromResponse(error);
            });
    }

    selectCategory()
    {
        if (!this.categories || this.categories.length < 1)
        {
            this.alertProvider.showMessage('Categories not found.');

            return;
        }

        const inputs = [];

        for (let i = 0; i < this.categories.length; i++)
        {
            inputs[i] = { type: 'radio', label: this.categories[i].category, value: this.categories[i] };
        }

        this.alertCtrl.create({
            title: 'Select a Category',
            inputs: inputs,
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel'
                },
                {
                    text: 'Ok',
                    handler: (category: ProductCategoryModel) =>
                    {
                        if (category)
                        {
                            this.selectedCategory = category;
                        }
                        else
                        {
                            this.alertProvider.showMessage('Category not found.');
                        }
                    }
                }
            ]
        }).present();
    }

    selectSubCategory()
    {
        if (this.selectedCategory.sub_categories.length < 1)
        {
            this.alertProvider.showMessage('Sub Categories not found.');

            return;
        }

        const inputs = [];

        for (let i = 0; i < this.selectedCategory.sub_categories.length; i++)
        {
            inputs[i] = {
                type: 'radio',
                label: this.selectedCategory.sub_categories[i].sub_category,
                value: this.selectedCategory.sub_categories[i]
            };
        }

        this.alertCtrl.create({
            title: 'Choose a Sub Category',
            inputs: inputs,
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel'
                },
                {
                    text: 'Ok',
                    handler: (subCategory: ProductSubCategoryModel) =>
                    {
                        if (subCategory)
                        {
                            this.selectedSubCategory = subCategory;
                        }
                        else
                        {
                            this.alertProvider.showMessage('Sub Category not found.');
                        }
                    }
                }
            ]
        }).present();
    }
}
