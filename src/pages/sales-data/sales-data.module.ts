import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { SalesDataPage } from './sales-data';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
    declarations: [SalesDataPage],
    imports: [ComponentsModule, IonicPageModule.forChild(SalesDataPage)],
})
export class SalesDataPageModule
{
}
