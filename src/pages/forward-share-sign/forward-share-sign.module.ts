import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { SignaturePadModule } from 'angular2-signaturepad';

import { ForwardShareSignPage } from './forward-share-sign';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
    declarations: [ForwardShareSignPage],
    imports: [ComponentsModule, SignaturePadModule, IonicPageModule.forChild(ForwardShareSignPage)],
})
export class ForwardShareSignPageModule
{
}
