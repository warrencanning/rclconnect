import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { StoreModel } from '../../models';

import { AlertProvider } from '../../providers';

@IonicPage()
@Component({
    selector: 'page-calls',
    templateUrl: 'calls.html',
})
export class CallsPage
{
    daysInWeek = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

    private stores: StoreModel[];

    constructor(private navCtrl: NavController,
                private navParams: NavParams,
                private alertProvider: AlertProvider)
    {
    }

    ionViewDidLoad()
    {
        this.stores = this.navParams.get('stores');
    }

    openProfilePage()
    {
        // this.navCtrl.push('ProfilePage');
    }

    openStoresPage(day: string)
    {
        const stores = this.stores.filter((store) => store.call.day.toLowerCase() === day.toLowerCase());

        if (stores && stores.length > 0)
        {
            this.navCtrl.push('StoresPage', { stores: stores });
        }
        else
        {
            this.alertProvider.showMessage(`There are no calls for ${day}`);
        }
    }
}
